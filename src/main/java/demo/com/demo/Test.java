package demo.com.demo;

public class Test {

	public static void main(String[] args) {
		int n = 5;
        for (int i = -n; i <= n; i++) {
            for (int j = -n; j <= n; j++) {
                if (i*i <= j*j) 
                	System.out.print("*");
                else           
                	System.out.print(" ");
            }
            System.out.println();
        }

	}

}
