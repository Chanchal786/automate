package demo.com.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FileRegularExpression {
	
	public static void xlsGen() {
		 XSSFWorkbook workbook = new XSSFWorkbook();
         
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet("Rev Sheet");
	          
	        //This data needs to be written (Object[])
	        Map<String, Object[]> data = new TreeMap<String, Object[]>();
	        data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
	        data.put("2", new Object[] {1, "Amit", "Shukla"});
	        data.put("3", new Object[] {2, "Lokesh", "Gupta"});
	        data.put("4", new Object[] {3, "John", "Adwards"});
	        data.put("5", new Object[] {4, "Brian", "Schultz"});
	          
	        //Iterate over data and write to sheet
	        Set<String> keyset = data.keySet();
	        int rownum = 0;
	        for (String key : keyset)
	        {
	            Row row = sheet.createRow(rownum++);
	            Object [] objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	               if(obj instanceof String)
	                    cell.setCellValue((String)obj);
	                else if(obj instanceof Integer)
	                    cell.setCellValue((Integer)obj);
	            }
	        }
	        try
	        {
	            FileOutputStream out = new FileOutputStream(new File("C:\\Bat_Output\\demo.xlsx"));
	            workbook.write(out);
	            out.close();
	            System.out.println("demo.xlsx written successfully on disk.");
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	}

	public static void formating(String inputFile) {
		try {
			// C:\Bat_Output\Test.txt
			File file = new File(inputFile);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "", oldtext = "";
			while ((line = reader.readLine()) != null) {
				oldtext += line + "\r\n";
			}
			reader.close();
			String newtext = oldtext.replaceAll("\\r\\n", "\t");
			String fileText = newtext.replaceAll("	D", "\r\n D:");
			String fileText1 = fileText.replaceAll(".*svn log -l 2 ", "");
			System.out.println(fileText);
			String st[] = fileText1.split("			");
			System.out.println(st.length);
			Map<String,String> map=new HashMap();
			for (int i = 0; i < st.length; i++) {
				Pattern patternRev = Pattern.compile("(.*?)(r\\d+)(\\d+?)");
				Matcher matcherRev = patternRev.matcher(st[i]);
				String str = "";
				String strURL=st[i].replaceAll(" 	-.*", "");
				System.out.println(strURL);
				StringBuilder stBuilder=new StringBuilder();
				int ct=0;
				String strRev="";
				while (matcherRev.find()) {
					str =matcherRev.group(2) + matcherRev.group(3) + " ";
					strRev=str+"  "+strRev;
				}
				System.out.println(strRev);
				map.put(strURL,strRev);
			}
			
			// System.out.println(revStr);
			// System.out.println(svnURL);
			FileWriter writer = new FileWriter("C:\\Bat_Output\\Test3.txt");
			writer.write(fileText1);
			// writer.write(fileText);
			// writer.write(newtext);
			writer.close();
			System.out.println("Done");
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			System.out.println("Terminated");
		}

	}

	public static void main(String[] args) {
		String input = "C:\\Bat_Output\\Test.txt";
		formating(input);
		xlsGen();
	}
}