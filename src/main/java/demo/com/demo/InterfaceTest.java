package demo.com.demo;

public class InterfaceTest implements My,My1{

	public void mm() {
		System.out.println("Interface test");
	}
	@Override
	public void mmmm() {
		My.super.mmmm();
	}
	public static void main(String[] args) {
		InterfaceTest ti=new InterfaceTest();
		ti.mm();
		ti.mmmm();
		My1 m=new InterfaceTest();
		m.mmmm();
	}
	/*
	 * public void mmmm() { System.out.println("int Test call");
	 * 
	 * }
	 */
}
